import React, { useEffect } from "react";
import { Box, Grid } from "@material-ui/core";
import { useAuthContext } from "../context/auth-context";
import LoginContainer from "../container/login-container";

export const HomeComponent = () => {
	const { user } = useAuthContext();

	useEffect(() => {
		document.title = "Home";
	}, []);

	return (
		<div
			style={{
				width: "100%",
				height: "calc(100% - 64px)",
				textAlign: "center",
			}}
		>
			<Box p={2}>
				<Grid container spacing={2}>
					<Grid item xs={12}>
						{user ? (
							<>
								<h1>Welcome {user ? "back " + user.username : "visitor"} !</h1>
								<img
									style={{ width: "100%", height: "70vh" }}
									src="/assets/images/welcome-back.svg"
									alt="welcome-back"
								/>
							</>
						) : (
							<LoginContainer />
						)}
					</Grid>
				</Grid>
			</Box>
		</div>
	);
};
