import React, {useContext, useState} from "react";
import {useCookies} from "react-cookie";

type Theme = "light" | "dark";
type ThemeContextType = {theme: Theme;};

export const ThemeContext = React.createContext<ThemeContextType>(
    {} as ThemeContextType
);

export const ThemeProvider: React.FC = ({children}) => {
    const [theme, setTheme] = useState<Theme>("light");

    return (
        <ThemeContext.Provider value={{theme}}>
            {children}
        </ThemeContext.Provider>
    );
};

export const useThemeContext = () => useContext(ThemeContext);