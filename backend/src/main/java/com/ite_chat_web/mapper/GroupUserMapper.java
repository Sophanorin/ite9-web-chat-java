package com.ite_chat_web.mapper;

import com.ite_chat_web.dto.GroupMemberDTO;
import com.ite_chat_web.entity.GroupUser;
import org.springframework.stereotype.Service;

@Service
public class GroupUserMapper {

    public GroupMemberDTO toGroupMemberDTO(GroupUser groupUser) {
        GroupMemberDTO dto = new GroupMemberDTO();
        dto.setAdmin(groupUser.getRole() == 1);
        if (groupUser.getUserMapping() != null) {
            dto.setUserId(groupUser.getUserMapping().getId());
            dto.setFirstName(groupUser.getUserMapping().getFirstName());
            dto.setLastName(groupUser.getUserMapping().getLastName());
        }
        return dto;
    }
}
