package com.ite_chat_web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WsNativeApplication {

	public static void main(String[] args) {
		SpringApplication.run(WsNativeApplication.class, args);
	}

}
